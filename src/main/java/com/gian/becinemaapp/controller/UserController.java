package com.gian.becinemaapp.controller;

import com.gian.becinemaapp.model.User;
import com.gian.becinemaapp.repository.UserRepository;
import com.gian.becinemaapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {

    @Autowired
 private UserService userService;

    @GetMapping("/profile")
    public User findUserByJwt(@RequestHeader("Authorization") String jwt) throws Exception {
        User user = userService.findUserByJwt(jwt);
        return user;
    }
}
