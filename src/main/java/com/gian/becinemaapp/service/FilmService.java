package com.gian.becinemaapp.service;

import com.gian.becinemaapp.model.Film;

import java.time.LocalDate;
import java.util.List;

public interface FilmService {
    List<Film> getFilmInProgrammazione();
    List<Film>getFilms();
    Film getFilmById(Long id);
}
