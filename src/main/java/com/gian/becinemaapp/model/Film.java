package com.gian.becinemaapp.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@Entity
public class Film {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String titolo;
    private String regista;
    private int durata;
    private String immagine;
    private String genere;
    private String descrizione;
    private LocalDate dataInizio;
    private LocalDate dataFine;
    private int numeroSala;

    // costruttore, getter e setter
}

