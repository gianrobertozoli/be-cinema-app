package com.gian.becinemaapp.repository;

import com.gian.becinemaapp.model.Film;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface FilmRepository extends JpaRepository<Film, Long> {
    List<Film> findAll();
    List<Film> findByDataInizioLessThanEqualAndDataFineGreaterThanEqual(LocalDate dataInizio, LocalDate dataFine);
}



