package com.gian.becinemaapp.service;

import com.gian.becinemaapp.model.User;

public interface UserService {
    public User findUserById(Long userId) throws Exception;
    public User findUserByJwt(String jwt) throws Exception;
}
