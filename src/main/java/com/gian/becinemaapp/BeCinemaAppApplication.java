package com.gian.becinemaapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BeCinemaAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(BeCinemaAppApplication.class, args);
	}

}
