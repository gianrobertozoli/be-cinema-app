package com.gian.becinemaapp.service;

import com.gian.becinemaapp.exception.FilmNotFoundException;
import com.gian.becinemaapp.model.Film;
import com.gian.becinemaapp.repository.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class FilmServiceImpl implements FilmService{

    @Autowired
    private FilmRepository filmRepository;

    @Override
    public List<Film> getFilmInProgrammazione() {
        LocalDate dataCorrente = LocalDate.now();
        return filmRepository.findByDataInizioLessThanEqualAndDataFineGreaterThanEqual(dataCorrente, dataCorrente);
    }

    @Override
    public List<Film> getFilms() {
        return filmRepository.findAll();
    }

    @Override
    public Film getFilmById(Long id) {
        return filmRepository.findById(id)
                .orElseThrow(() -> new FilmNotFoundException("Film non trovato con ID: " + id));
    }

}
